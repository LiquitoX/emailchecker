<?php
  // Start session
  session_start();
  // Get file to work with.
  if(isset($_POST['file'])) {
    //$mailFile = file_get_contents($_POST['file'].'.txt');
    $mailFile = $_POST['file'];

    // Put all characters that need to be replaced in an array.
    $replaceChars = array(' ', '?', '"', "'", ';', '[', ']', '%20', '+');

    // Replace all characters from above in the string.
    foreach ($replaceChars as $replaceChar){
      $mailFile = str_replace($replaceChar, '', $mailFile);
    }

    // Create array $emails, while also removing all other whitespaces.
    $emails = array_map('trim', array_filter(explode("\n", $mailFile)));

    // This function checks if the presented email is a valid email, if jes, write it to the file.
    function checkEmail($emails = array())
    {
      // Initialize empty array for all correct emails.
      $correctEmails = array();

      // Initialize empty array for all bad emails.
      $badEmails = array();

      // Filter all emails.
      foreach ($emails as $mail) {
        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
          $correctEmails[] = $mail;
        } else {
          $badEmails[] = $mail;
        }
      }
      // set session variables.
      $_SESSION['correctEmails'] = $correctEmails;
      $_SESSION['badEmails'] = $badEmails;
      echo count($correctEmails) . " emails accepted! Click <a href='correctEmails.php'>here</a> to see them all<br>";
      echo count($badEmails) . " emails Rejected! Click <a href='badEmails.php'>here</a> to see them all<br>";
      echo "<p>Click <a href='./checkEmails.php'>here</a> to return.</p>";
    }
    checkEmail($emails);
  } else {
?>
  <form action="./checkEmails.php" method="POST">
    <label for="file">Please put any number of emails in the textbox below. Every email should start on a new line.</label><br>
    <textarea name="file" id="file" cols="30" rows="10" autofocus></textarea>
    <br>
    <input type="submit" value="Check Emails">
  </form>
  <p>Project is hosted on <a href="https://gitlab.com/LiquitoX/emailchecker">GitLab</a></p>
<?php
  }
?>