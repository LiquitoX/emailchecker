# EmailChecker by LiquitoX

###### Idea originated from @MonkeyAgoo, as she was working with 40k+ emails and I wanted to help her a bit.

#### Deployment:
Just upload the files to your webserver and enjoy using the predetermined characters that can be replaced.

#### Configuration: 
I've set an array with all characters that are usually in an emailList. You can change this to your liking, just add or remove entries in the array called $replaceChars in checkEmails.php

#### License:
None really, do with it what you want.

